package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class Message {
    public final boolean isError;
    public final String text;

    public Message(boolean isError, String text) {
        this.isError = isError;
        this.text = text;
    }
}
