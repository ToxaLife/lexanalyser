package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class Scanner {
    public final String program;

    private Compiler compiler;
    private Position cur;

    public Scanner(String program, Compiler compiler) {
        this.compiler = compiler;
        this.program = program;
        cur = new Position(program);
    }

    public Token nextToken() {
        while (cur.Cp() != -1) {
            while (cur.isWhiteSpace()) { cur.next(); }
            Position start = cur.copy();

            switch (cur.Cp()) {
                case 'e':case 'y':case 'u':case 'i':case 'o':case 'a':
                    boolean numberInIdent = false;
                    while (cur.isLetterOrDiggit()) {
                        if (cur.isDecimialDigit()) {
                            numberInIdent = true;
                        }
                        cur.next();
                    }
                    if (!numberInIdent) {
                        String name = program.substring(start.getIndex(), cur.getIndex());
                        return new IdentToken(compiler.addName(name), start, cur);
                    } else {
                        compiler.addMessage(true, cur.copy(), "Bad IDENT name: must not contain numbers");
                        break;
                    }
                case'0': case '1':case '2': case '3':case '4':case '5':case '6':case '7':case '8':case '9':
                    StringBuilder number = new StringBuilder();
                    boolean letterInNumber = false;
                    while (cur.isLetterOrDiggit()) {
                        if (cur.isLetter()) {
                            letterInNumber = true;
                        }

                        number.append(Character.toChars(cur.Cp()));
                        cur.next();
                    }
                    if (!letterInNumber) {
                        String value = number.toString();
                        return new NumberToken(Integer.valueOf(value), start, cur);
                    } else {
                        compiler.addMessage(true, cur.copy(), "Bad NUMBER value: must not contain letters");
                        break;
                    }
                case '-':
                    cur.next();
                    if (cur.isDecimialDigit()) {
                        number = new StringBuilder();
                        number.append("-");
                        letterInNumber = false;
                        while (cur.isLetterOrDiggit()) {
                            if (cur.isLetter()) {
                                letterInNumber = true;
                            }
                            number.append(Character.toChars(cur.Cp()));
                            cur.next();
                        }
                        if (!letterInNumber) {
                            String value = number.toString();
                            return new NumberToken(Integer.valueOf(value), start, cur);
                        } else {
                            compiler.addMessage(true, cur.copy(), "Bad NUMBER value: must not contain letters");
                            break;
                        }
                    } else if (cur.Cp() == '-') {
                        cur.next();
                        return new SpecToken(DomainTag.DECREMENT, start, cur);
                    } else {
                        compiler.addMessage(true, cur.copy(), "Unrecognized minus");
                        break;
                    }
                case '<':
                    cur.next();
                    if (cur.Cp() == '=') {
                        cur.next();
                        return new SpecToken(DomainTag.LESSOREQUAL, start, cur);
                    } else {
                        //cur.next();
                        return new SpecToken(DomainTag.LESS, start, cur);
                    }
                default:
                    if (cur.isLetter()) {
                        while (cur.isLetterOrDiggit()) {
                            cur.next();
                        }
                        compiler.addMessage(true, cur.copy(), "Bad IDENT name: must start with vowel letter");
                        cur.next();
                        break;
                    } else {
                        compiler.addMessage(true, cur.copy(), "Unrecognized symbol");
                        cur.next();
                    }
            }
        }
        return new SpecToken(DomainTag.END_OF_PROGRAM, cur, cur);
    }
}
