package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class IdentToken extends Token {
    public final int code;

    public IdentToken(int code, Position starting, Position following) {
        super(DomainTag.IDENT, starting, following);
        this.code = code;
    }

}
