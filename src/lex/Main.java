package lex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String myProgram = new String(Files.readAllBytes(Paths.get("/Users/ToxaLife/Google Drive/МГТУ/6 семестр/Конструирование компиляторов/Лабы/Лаба 4 (Объктно-ориентированный лексический анализатор)/src/program")));
        Compiler myCompiler = new Compiler();
        Scanner myScanner = myCompiler.getScanner(myProgram);

        System.out.println("PROGRAM");
        System.out.println(myScanner.program);

        System.out.println("TOKENS");

        Token token = myScanner.nextToken();
        while(token.tag != DomainTag.END_OF_PROGRAM) {
            System.out.print(token);
            if (token instanceof IdentToken) {
                System.out.println(" " + myCompiler.getName(((IdentToken)token).code));
            } else if (token instanceof NumberToken){
                System.out.println(" " + ((NumberToken)token).value);
            } else {
                System.out.println("");
            }
            token = myScanner.nextToken();
        }
        System.out.println("MESSAGES");
        myCompiler.outputMessages();

    }
}
