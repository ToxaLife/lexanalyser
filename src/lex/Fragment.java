package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class Fragment {
    public final Position starting, following;

    public Fragment(Position starting, Position followng) {
        this.starting = starting;
        this.following = followng;
    }

    @Override
    public String toString() {
        return starting.toString() + "-" + following.toString();
    }


}
