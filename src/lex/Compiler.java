package lex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class Compiler {
    private TreeMap<Position, Message> messages;
    private HashMap<String, Integer> nameCodes;
    private ArrayList<String> names;

    public Compiler() {
        messages = new TreeMap<Position, Message>();
        nameCodes = new HashMap<String, Integer>();
        names = new ArrayList<String>();
    }

    public int addName(String name) {
        if (nameCodes.containsKey(name)) { return nameCodes.get(name); }
        else {
            int code = names.size();
            names.add(name);
            nameCodes.put(name, code);
            return code;
        }
    }

    public String getName(int code) {
        return names.get(code);
    }

    public void addMessage(boolean isErr, Position c, String text) {
        messages.put(c, new Message(isErr, text));
    }

    public void outputMessages() {
        for (Map.Entry<Position, Message> p : messages.entrySet()) {
            System.out.print(p.getValue().isError ? "Error" : "Warning");
            System.out.print(" " + p.getKey() + ": ");
            System.out.println(p.getValue().text);
        }
    }

    public Scanner getScanner(String program) {
        return new Scanner(program, this);
    }
}
