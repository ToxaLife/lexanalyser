package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public enum DomainTag {
    STRING, NUMBER, IDENT, DECREMENT, LESS, LESSOREQUAL, END_OF_PROGRAM
}
