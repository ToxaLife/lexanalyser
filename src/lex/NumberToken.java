package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class NumberToken extends Token {
    public final long value;


    public NumberToken(long val, Position starting, Position following) {
        super(DomainTag.NUMBER, starting, following);
        value = val;
    }
}
