package lex;

/**
 * Created by ToxaLife on 29/03/15.
 */
public class SpecToken extends Token {
    public final DomainTag tag;
    public SpecToken(DomainTag tag, Position starting, Position following) {
        super(tag, starting, following);
        this.tag = tag;
    }
}
