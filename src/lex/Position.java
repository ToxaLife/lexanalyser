package lex;


/**
 * Created by toxalife on 26/03/15.
 */

public class Position implements Comparable<Position> {
    private String text;
    private int line, pos, index;

    public Position(String text) {
        this.text = text;
        line = pos = 1;
        index = 0;
    }

    public Position copy(){
        Position p = new Position(this.text);
        p.text = this.text;
        p.line = this.line;
        p.pos = this.pos;
        p.index = this.index;
        return p;
    }

    public String getText() { return text; }
    public int getLine() { return line; }
    public int getPos() { return pos; }
    public int getIndex() { return index; }

    public void setText(String text) { this.text = text; }
    public void setLine(int line) { this.line = line; }
    public void setPos(int pos) { this.pos = pos; }
    public void setIndex(int index) {this.index =  index; }

    public int Cp() {
        return (index == text.length()) ? -1 : Character.codePointAt(text, index);
    }
    public String Uc() {
        return Character.getName(Character.codePointAt(text, index));
    }

    public boolean isWhiteSpace() {
        return index != text.length() && Character.isWhitespace(text.charAt(index));
    }

    public boolean isLetter() {
        return index != text.length() && Character.isLetter(text.charAt(index));
    }

    public boolean isLetterOrDiggit() {
        return index != text.length() && Character.isLetterOrDigit(text.charAt(index));
    }

    public boolean isDecimialDigit() {
        return index != text.length() && text.charAt(index) >= '0' && text.charAt(index) <= '9';
    }

    public boolean isNewLine() {
        if (index == text.length()) {
            return true;
        }
        if (text.charAt(index) == '\r' && index + 1 < text.length()) {
            return (text.charAt(index + 1) == '\n');
        }
        return (text.charAt(index) == '\n');
    }

    @Override
    public int compareTo(Position o) {
        if (index > o.getIndex()) {
            return 1;
        } else if (index < o.getIndex()){
            return -1;
        }
        return 0;
    }

    public boolean hasNext() {
        return index < text.length();
    }

    public Position next() {

        if(hasNext()) {
            if (isNewLine()) {
                if(text.charAt(index) == '\r') { index++; }
                line++;
                pos = 1;
            } else {
                if (Character.isHighSurrogate(text.charAt(index))) { index++; }
                pos++;
            }
            index++;
        }
        return this;
    }

    @Override
    public String toString() {
        return "(" + line + "," + pos + ")";
    }
}
